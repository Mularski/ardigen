from unittest import mock, TestCase
from ardigen2 import calculate_unit_price, get_row_data
import pandas as pd



class SecondTaskTestCase(TestCase):

    currencies = pd.Series(index=['GBP', 'EU', 'PLN'], data=[2.4, 2.1, 1.0])
    data = pd.DataFrame({
        'id': [1, 2],
        'price': [100, 1050],
        'currency': ['GBP', 'EU'],
        'quantity': [2, 1],
        'matching_id': [3, 1],
        'total_price': [4800, 2205],
        'unit_price': [2400, 2205],
    }, columns=['id', 'price', 'currency', 'quantity', 'matching_id', 'total_price', 'unit_price'])

    def test_calculate_unit_price(self):
        labels = ['id', 'price', 'currency', 'quantity', 'matching_id']
        values = [1, 1000, 'GBP', 2, 3]
        row = pd.Series(index=labels, data=values)
        
        result = calculate_unit_price(row)
        expected = 2.4 * 1000
        self.assertEqual(result, expected)

    def test_get_row_data(self):
        row = pd.Series(index=['matching_id', 'top_priced_count'], data=[1, 2])
        expected = (12285.0, 2782.5, 'GBP', 1)
        result = get_row_data(row)
        self.assertEqual(expected, result)

