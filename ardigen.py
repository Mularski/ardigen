def print_fizz(n: int, m: int):
    if n > m:
        raise Exception("First integer should be lower than second")
    elif n < 1 or m > 10000:
        raise Exception("Integers should be between 1 and 10000")

    for idx in range(n, m + 1):
        output = ''
        if idx % 3 == 0:
            output = "Fizz"
        if idx % 5 == 0:
            output += "Buzz"

        print(output if output else idx)


if __name__ == 'main':
    print('Enter your first number:')
    n = input()
    print('Enter your second number:')
    m = input()

    print_fizz(int(n), int(m))
