import io

from unittest import mock, TestCase
from ardigen import print_fizz

class FirstTaskTestCase(TestCase):

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def assert_stdout(self, func, args, expected_output, mock_stdout):
        func(*args)
        self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_small_numbers(self):
        self.assert_stdout(print_fizz, (3, 6), 'Fizz\n4\nBuzz\nFizz\n')

    def test_big_numbers(self):
        expected = 'Fizz\n4\nBuzz\nFizz\n7\n8\nFizz\nBuzz\n11\nFizz\n13\n14\nFizzBuzz\n16\n'
        self.assert_stdout(print_fizz, (3, 16), expected)

    def test_number_first_should_be_smaller(self):
        error_msg = "First integer should be lower than second"
        self.assertRaisesRegex(Exception, error_msg, print_fizz, 6, 3)

    def test_numbers_should_be_between_two_values__too_small(self):
        error_msg = "Integers should be between 1 and 10000"
        self.assertRaisesRegex(Exception, error_msg, print_fizz, 0, 3)

    def test_numbers_should_be_between_two_values__to_big(self):
        error_msg = "Integers should be between 1 and 10000"
        self.assertRaisesRegex(Exception, error_msg, print_fizz, 1, 123456789)
