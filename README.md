# App description
The first file (ardigen.py) contains solution for the first task and the second one for second. The first script prints numbers and strings according to the task description. When run you will be prompted to select two numbers which are then used by the algorithm.

Second script (ardigen2.py) takes as an input three files: currencies.csv, data.csv and matchings.csv and outputs file called "top_products" which holds information about products and their prices. Columns: 
matching_id - product's id 
total_price - summarised cost of all the products of certain matching_id of the data.csv subset limited by matchings.csv top_priced_count in PLN 
avg_price - average price of a single unit of a product in PLN 
currency - currency in which the most expensive units of a product were sold ignored_products_count - number of products that were not taken into consideration (excluded by the top_priced_count from matchings.csv) in calculations


## Running the project
To run the project simply type: 
`python ardigen.py` or 
`python ardigen2.py` 

## Running tests
To run tests: 
`pytest test_csvreader.py` or 
`pytest test_printfizz.py` 

## Project requirements
-Python3.7 or above

-Python modules used to run the project:

`numpy, pandas, pytest and their own requirements.`

To install them all **create new python virtual environment.**

`python3 -m venv env`

**Activate the environment.**

Ubuntu:

`source env/bin/activate`

Windows:

`env/Scripts/activate.bat`

And finally **install the needed python modules**:

`pip install -r requirements.txt`
