import pandas as pd
import os


def read_files():
    currencies = pd.read_csv('currencies.csv')
    matchings = pd.read_csv('matchings.csv')
    data = pd.read_csv('data.csv')
    return currencies, matchings, data


currencies, matchings, data = read_files()
output_path = 'top_products.csv'

def calculate_unit_price(row):
    currency_row = currencies.loc[currencies['currency'] == row['currency']]
    currency = currency_row.values[0][1]
    return row['price'] * currency

def get_row_data(row):
    req_data = data.loc[data['matching_id'] == row['matching_id']]
    sorted = req_data.sort_values('total_price', ascending=False).head(row['top_priced_count'])
    
    ignored_products_count = len(req_data.index) - len(sorted.index)
    price_sum = sorted['total_price'].aggregate('sum')  # == sum(sorted['total_price'])
    avg_price = sorted['unit_price'].aggregate('mean')
    highest_unit_price_row = sorted.loc[sorted['total_price'] == sorted['total_price'].aggregate('max')]
    currency = highest_unit_price_row.values[0][2]
    return price_sum, avg_price, currency, ignored_products_count

def get_top_products():
    global data
    total_prices = [calculate_unit_price(row) * row['quantity'] for idx, row in data.iterrows()]
    unit_prices = [calculate_unit_price(row) for idx, row in data.iterrows()]
    
    data = data.assign(total_price = pd.Series(total_prices).values)
    data = data.assign(unit_price = pd.Series(unit_prices).values)

    results = pd.DataFrame(columns=['matching_id', 'total_price', 'avg_price', 'currency', 'ignored_products_count'])
    
    for idx, row in matchings.iterrows():
        price_sum, avg_price, currency, ignored_products_count = get_row_data(row)
        results.loc[idx] = [row['matching_id'], price_sum, avg_price, currency, ignored_products_count]

    os.remove(output_path) if os.path.isfile(output_path) else ''
    results.to_csv(output_path)

get_top_products()
